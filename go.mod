module blot

go 1.19

require (
	github.com/dlclark/regexp2 v1.7.0
	github.com/fatih/color v1.13.0
	github.com/spf13/cobra v1.6.1
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/mattn/go-colorable v0.1.9 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.0.0-20220310020820-b874c991c1a5 // indirect
)
