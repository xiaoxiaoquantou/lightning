package base

import (
	"fmt"
	"time"
)

type Bar struct {
	current int       //当前进度
	percent int       //百分比
	take    time.Time //耗时
	total   int       //总进度
	grah    string    //进度条符号
	rate    string    //进度条
}

func NewBar(total int) *Bar {
	bar := Bar{
		current: 0,
		total:   total,
		grah:    "█",
	}
	bar.rate = bar.grah
	return &bar
}

func (bar *Bar) Load() {

	bar.current += 1
	a := bar.Precent()
	if int(a)%2 == 0 {
		bar.rate = ""
		for i := 2; i < int(a); i += 2 {
			bar.rate += bar.grah
		}

	}

	fmt.Printf("\r[%-49s]  %.2f%%", bar.rate, a)

}

func (bar *Bar) Precent() float32 {
	//计算进度

	return 100 * (float32(bar.current) / float32(bar.total))
}
