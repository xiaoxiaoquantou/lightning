# lightning

#### 介绍
我在学习过程当中，面对众多的功能，不能及时有效的提取目标有效信息。所以我开发了这款提取目标的url，js，其他域名(子域名)，能够及时快速的发现敏感接口。采用go开发，利用协程提取信息，快人一步的发现有效信息(一般网站可以做到毫秒级别)。同时我采用半框架式，封装了基础提取，所以不局限于提取敏感信息发掘，比如还能拓展批量搜索，针对post请求的漏洞测试。所以后期我会继续拓展此框架的功能。同时感兴趣的小伙伴也可以拓展.信息收集利器！！！ 采用chatgpt优化了正则表达式，速度更快。**此开源只做学习交流。严禁用于非法用途！** 

#### 使用说明
windows用户可以直接利用lightning.exe，linux用户需要采用命令go build lightning.go打包成二进制文件。
### 【2023.3.6】
由于ChatGpt开放了接口，准备将ChatGpt引入到工具中，对某些遗忘知识快速查询学习。为了方便复制某些内容采用pyqt开发对话框，利用golang的gin框架作为后端
### 【2023.2.20】
引入context，更好的控制并发和分配资源消耗
### 【2023.2.3】
我觉得采用dirbuster速度太慢了，所以我想利用dirbuster的字典和现有框架进行新开发一个目录扫描器。接入到现有的前端文档当中。经测试3w多数据在20s执行完毕

### 【命令说明】：
-  **提取目标中的url** 

```
lightning.exe JsFind --u http://www.baidu.com
```

- --i 生成html文档(lightning.exe --u xxx.com --i 生成html的名称 会生成在tem文件下，默认生成defalut.html)

```
lightning.exe JsFind --u http://www.baidu.com --i name
```

- --s cmd中显示详细信息

```
lightning.exe JsFind --u http://www.baidu.com --s
```

- --headers 设置请求头和请求头中的认证信息

```
lightning.exe JsFind --u http://www.baidu.com --headers  cookIE(头内容):cookie=xxxxx（值内容）多个值用;分割
```

- --t 设置线程数,默认30

```
lightning.exe JsFind --u http://www.baidu.com --t 10,设置十个请求线程
```

- --timeout 设置每个请求的超时时间，默认3秒 

```
lightning.exe JsFind --u http://www.baidu.com --timeout 10 
```
-  **目录爆破** 
```
lightning.exe Dir--u http://www.baidu.com
```

- --i 生成html文档

```
lightning.exe Dir--u http://www.baidu.com --i name
```

- --headers 设置请求头和请求头中的认证信息

```
lightning.exe Dir--u http://www.baidu.com --headers  cookIE(头内容):cookie=xxxxx（值内容），多个值用;分割
```

- --t 设置线程数，默认30线程

```
lightning.exe Dir--u http://www.baidu.com --t 10,设置十个请求线程
```

- --timeout 设置每个请求的超时时间，默认3秒

```
lightning.exe Dir --u http://www.baidu.com --timeout 10 
```

- --sleep 设置目录扫描时每个线程请求之间的时间间隔（比如10个线程sleep=1，那么就是1秒钟10个请求）

```
lightning.exe Dir --u http://www.baidu.com --sleep 1
```

- --w 设置其他的fuzz字典，可设置多个以分号（；）分割

```
lightning.exe Dir --u http://www.baidu.com --w ./user/wordlist.txt
```
- --ex 设置需要添加的后缀名，多个以逗号（，）分割

```
lightning.exe Dir --u http://www.baidu.com --ex php,jsp
```
- --status 指定状态码，默认200,301,302,403,多个状态码以逗号分割

```
lightning.exe Dir --u http://www.baidu.com --status 404,500
```

- --filter由于一些网站访问中实际是404，但是不会再状态码中返回而是通过内容返回，所以通过数据包长度判断是否是相同的404内容，但是这不可避免的会出现丢失部分数据

```
lightning.exe Dir --u http://www.baidu.com --filter 过滤数据的大小
```

- --version 查看版本
lightning.exe --version
- --help 查看帮助
lightning.exe --help
- 识别敏感url的字典在fuzz下的js.txt中，有需求的小伙伴可以自行配置
- url提取：

![输入图片说明](css/src/img/url1.png)
![输入图片说明](css/src/img/url2.png)
- 目录爆破：

![输入图片说明](css/src/img/%E5%9B%BE%E7%89%87.png)
![输入图片说明](css/src/img/ml2.png)
#### 参与贡献
感谢我的好友雪见对生成文档的部分前端帮助

