package structural

import (
	"blot/config"
	"fmt"
	"io"
	"time"
)

type YamlData struct {
	Data map[any]any
}

var Yaml_data = YamlData{config.Read_config()}.Data

var Useraget = fmt.Sprintf("%s", Yaml_data["headers"].(map[any]any)["user-agent"])

type UrlData struct {
	Url     string
	Domname string
}
type Headers struct {
	Key   string
	Value string
}

type Setting struct {
	Timeout time.Duration
	Method  string
	Header  []Headers
	Host    string
}

type Body struct {
	Data io.Reader
}

var (
	U       string //url链接
	I       string //生成模板
	S       bool   //详细信息
	T       int    //线程数
	H       string //请求头
	Html    = make(chan string, 1)
	Timeout int //请求超时时间
	URL     UrlData
	Sleep   int
	W       string
	Filter  int
	Ex      string
	Status  string
)
