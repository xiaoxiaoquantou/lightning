package cmd

//
import (
	"blot/structural"
	"github.com/spf13/cobra"
	"os"
	"strings"
)

func init() {
	rootcommod.Version = "1.2.0"
	rootcommod.PersistentFlags().StringVar(&structural.U, "u", "", "目标URL")
	rootcommod.PersistentFlags().IntVar(&structural.T, "t", 30, "线程数量")
	rootcommod.PersistentFlags().StringVar(&structural.I, "i", "defalut", "生成文档的名称")
	rootcommod.PersistentFlags().StringVar(&structural.H, "headers", "", "添加请求头包括请求头中的认证字段")
	rootcommod.PersistentFlags().BoolVar(&structural.S, "s", false, "显示在cmd中")
	rootcommod.PersistentFlags().IntVar(&structural.Sleep, "sleep", 0, "延迟时间")
	rootcommod.PersistentFlags().IntVar(&structural.Timeout, "timeout", 3, "超时时间")

}

var (
	rootcommod = cobra.Command{
		Use:   "lightning",
		Short: "根命令",
	}
)

func Execute() {

	if err1 := rootcommod.Execute(); err1 != nil {
		os.Exit(1)
	}
}

func SetHeader(s *structural.Setting) {
	//处理请求头

	if structural.H == "" {
		return
	}
	headers := strings.Split(structural.H, ";")
	var h structural.Headers
	for _, data := range headers {
		values := strings.Split(data, ":")
		h.Key = values[0]
		h.Value = values[1]
		s.Header = append(s.Header, h)
	}

}
