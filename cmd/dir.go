package cmd

import (
	"blot/base"
	"blot/dir"
	"blot/structural"
	"github.com/spf13/cobra"
	"strings"
	"time"
)

var Dir = &cobra.Command{
	Use:   "Dir",
	Short: "fuzz目录",
	Run:   StarDir,
}

func StarDir(cmd *cobra.Command, args []string) {
	var errurl error
	dir.Ex = strings.Split(structural.Ex, ",")
	dir.Status = strings.Split(structural.Status, ",")

	if structural.URL.Url, structural.URL.Domname, errurl = base.UrlDispose(structural.U); errurl != nil {
		panic(errurl)
	}
	var s = structural.Setting{
		Timeout: time.Duration(structural.Timeout) * time.Second,
		Method:  "GET",
		Host:    structural.URL.Domname,
	}
	SetHeader(&s)
	dir.DirHttp = base.NewHTTPClient(s)
	dir.StartDir()
}
func init() {
	Dir.Flags().StringVar(&structural.W, "w", "./fuzz/dir.txt", "dir字典路径")
	Dir.Flags().IntVar(&structural.Sleep, "sleep", 0, "一个协程中每个请求的间隔时间")
	Dir.Flags().IntVar(&structural.Filter, "filter", 0, "通过数据包大小过滤一些无用数据")
	Dir.Flags().StringVar(&structural.Ex, "ex", "", "添加字典的后缀名")
	Dir.Flags().StringVar(&structural.Status, "status", "200,301,302,403", "指定状态码，默认200,301,302,403")
	rootcommod.AddCommand(Dir)

}
