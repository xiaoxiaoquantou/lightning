package cmd

import (
	"blot/base"
	"blot/jsfind"
	"blot/structural"
	"github.com/spf13/cobra"
	"time"
)

var Jsfind = &cobra.Command{
	Use:   "JsFind",
	Short: "提取目标网站中的url",
	Run:   StartJsfind,
}

func StartJsfind(cmd *cobra.Command, args []string) {
	//项目入口
	var errurl error
	if structural.URL.Url, structural.URL.Domname, errurl = base.UrlDispose(structural.U); errurl != nil {
		panic(errurl)
	}

	var html string
	//var context context2.Context
	var s = structural.Setting{
		Timeout: time.Duration(structural.Timeout) * time.Second,
		Method:  "GET",
	}
	SetHeader(&s)
	jsfind.JsRequest = base.NewHTTPClient(s)
	jsfind.JsRequest.RequestScan(structural.U, structural.Body{}).Scan(&html)
	structural.Html <- html
	jsfind.Start()
}

func init() {
	rootcommod.AddCommand(Jsfind) //命令需要先加载才能执行后续命令。所以利用init先加载
}
