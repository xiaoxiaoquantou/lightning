package main

import (
	"blot/cmd"
	"fmt"
	"github.com/fatih/color"
	"time"
)

func init() {

	color.Blue("  _ _       _     _         _             \n | (_)     | |   | |       (_)            \n | |_  __ _| |__ | |_ _ __  _ _ __   __ _ \n | | |/ _` | '_ \\| __| '_ \\| | '_ \\ / _` |\n | | | (_| | | | | |_| | | | | | | | (_| |\n |_|_|\\__, |_| |_|\\__|_| |_|_|_| |_|\\__, |\n       __/ |                         __/ |\n      |___/                         |___/ ")
	color.Yellow("名称：闪电")
	color.Yellow("版本: 1.2.0")
	color.Yellow("语言: Go 1.20")
	color.Yellow("=====================================")

}

func main() {
	defer func() {
		if err := recover(); err != nil {
			//color.Red("发生错误%w", err)
			color.Red(fmt.Sprintf("%s", err))
		}
	}()
	color.Green("收集中.....")

	a := time.Now()
	cmd.Execute()
	fmt.Println("所用耗时：", time.Since(a))
	color.Green("收集完成！！！")

}
